#t03_02_05_Restaurante

import os
from t03_02_04_Menu import *

def clear():
	if os.name == "nt":
		os.system("cls")
	elif os.name == "posix":
		os.system("clear")

def generar_platos():
	platos = []
	platos.append(Entrante("Tosta de piquillos caramelizados"))
	platos.append(Entrante("Mejillones a la cerveza con bacon"))
	platos.append(Entrante("Croquetas de atún"))
	platos.append(Pescado("Salmón con salteado de verduras"))
	platos.append(Pescado("Atún encebollado"))
	platos.append(Pescado("Rape con mejillones en salsa"))
	platos.append(Carne("Pollo al estragón"))
	platos.append(Carne("Pollo asado"))
	platos.append(Carne("Milanesa de ternera"))
	return platos

def print_tittle():
	clear()
	print("{0} RESTAURANTE {0}".format(":"*20))
	print()

def print_opciones():
	print("1. Nuevo Plato")
	print("2. Borrar Plato")
	print("3. Listar Platos")
	print("4. Generar Menú del día")
	print("5. Salir")
	print()

def _input(text="> "):
	try:
		opcion = input(text)
		return opcion
	except:
		print("Bye!")
		exit()

def nuevo_plato_tipo():
	print("\nElige tipo [E,P,C]:")
	tipo = _input()
	if tipo.upper() in ["E","P","C"]:
		return tipo.upper()
	else:
		return 0

def nuevo_plato(menu,tipo):
	print("\nNombre del plato:")
	nombre = _input()
	if tipo == "E":
		plato = Entrante(nombre)
	elif tipo == "P":
		plato = Pescado(nombre)
	elif tipo == "C":
		plato = Carne(nombre)
	menu.add_plato(plato)

def borrar_plato(menu):
	print("\nNombre del plato:")
	nombre = _input()
	menu.del_plato(nombre)

def main():
	platos = generar_platos()
	menu = Menu(platos)
	opcion = 0
	while 1:
		print_tittle()
		print_opciones()
		if opcion:
			print("> {}".format(opcion))
		else:
			opcion = _input()
			if opcion.isdigit():
				opcion = int(opcion)
			else:
				opcion = 0
		
		if opcion in range(1,6):
			opcion = int(opcion)
			if opcion == 1:
				tipo = nuevo_plato_tipo()
				if tipo:
					nuevo_plato(menu,tipo)
					opcion = 0
			elif opcion == 2:
				borrar_plato(menu)
				opcion = 0
			elif opcion == 3:
				print()
				menu.show_platos()
				_input("\nPulsa INTRO para continuar...")
				opcion = 0
			elif opcion == 4:
				print()
				menu.gen_menu()
				_input("\nPulsa INTRO para continuar...")
				opcion = 0
			elif opcion == 5:
				print("\nBye")
				exit()
		else:
			opcion = 0

if __name__ == "__main__":
	main()


