#t03_02_01_IntSet

#<INTSET>
class IntSet:
	
	#self, data_structure => None
	def __init__(self,iset=set()):
		if type(iset) in (tuple,list,set):
			int_set = (type(x) is int for x in iset)
			if all(int_set):
				self.cset = set(iset)
			else:
				raise ValueError("El conjunto solo puede contener "
								"números enteros")
		else:
			raise ValueError("El conjunto debe crearse a partir de una "
								"lista, tupla o otro conjunto")

	#self, int => None
	def insert(self,num):
		if type(num) is int and num not in self.cset:
			self.cset.add(num)
		else:
			raise ValueError("El conjunto solo puede contener números "
								"enteros")
	
	#self, int => None
	def remove(self,num):
		if num in self.cset:
			self.cset.remove(num)
		else:
			raise KeyError("No existe ese elemento en el conjunto")

	#self => set
	def copy(self):
		return self.cset[:]
		
	#self => tuple
	def get_set(self):
		return sorted(tuple(self.cset))
		
	#self, IntSet => set
	def union(self, other):
		if type(other) is IntSet:
			return IntSet(self.cset | other.cset)
		else:
			raise ValueError("Debe ser un objeto <IntSet>")
	
	#self, IntSet => set
	def intersect(self, other):
		if type(other) is IntSet:
			return IntSet(self.cset & other.cset)
		else:
			raise ValueError("Debe ser un objeto <IntSet>")
			
	#self, IntSet => set
	def subtract(self, other):
		if type(other) is IntSet:
			return IntSet(self.cset - other.cset)
		else:
			raise ValueError("Debe ser un objeto <IntSet>")
		
	#self => None
	def clear(self):
		self.cset = set()
	
	#self => string
	def __str__(self):
		return ("{{"+", ".join(["{}"]*len(self.get_set()))+
				"}}").format(*self.get_set())
		
	#self => Boolean
	def __eq__(self, other):
		return self.cset == other.cset
	
	#self => int
	def __len__(self):
		return len(self.cset)
		
	#self => iterator
	def __iter__(self):
		return iter(self.get_set())
		
		
