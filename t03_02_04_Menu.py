#t03_02_04_Menu

import random
		
#<PLATO>
class Plato:
	def __init__(self,name):
		self.name = name
		self.tag = "-"
	def __str__(self):
		return "({}) - {}".format(self.tag,self.name)

#<PLATO> => <ENTRANTE>
class Entrante(Plato):
	def __init__(self,name):
		super().__init__(name)
		self.tipo = "Entrante"
		self.tag = "E"

#<PLATO> => <PESCADO>
class Pescado(Plato):
	def __init__(self,name):
		super().__init__(name)
		self.tipo = "Pescado"
		self.tag = "P"

#<PLATO> => <CARNE>
class Carne(Plato):
	def __init__(self,name):
		super().__init__(name)
		self.tipo = "Carne"
		self.tag = "C"

#<MENU>
class Menu:
	
	def __init__(self,platos=[]):
		if all([issubclass(type(x),Plato) for x in platos]):
			self.platos = platos
			self.dict_platos = {}
			self.update_dict_platos()
		else:
			raise ValueError("El argumento debe ser un conjunto de Platos")

	def update_dict_platos(self):
		self.dict_platos = {"E":[],"P":[],"C":[]}
		for p in self.platos:
			self.dict_platos[p.tag].append(p)

	def add_plato(self, plato):
		self.platos.append(plato)
		self.update_dict_platos()

	def del_plato(self, name):
		for p in self.platos:
			if name == p.name:
				self.platos.remove(p)
		self.update_dict_platos()

	def show_platos(self):
		print("ENTRANTES")
		for p in self.dict_platos["E"]:
			print("\t{}".format(p))
		print("PESCADOS")
		for p in self.dict_platos["P"]:
			print("\t{}".format(p))
		print("CARNES")
		for p in self.dict_platos["C"]:
			print("\t{}".format(p))

	def gen_menu(self):
		if all(len(tl)>=2 for tl in self.dict_platos.values()):
			print("MENÚ")
			print("Primeros:")
			for e in random.sample(self.dict_platos["E"],2):
				print("\t{}".format(e))
			print("Segundos:")
			print("\t{}".format(random.choice(self.dict_platos["C"])))
			print("\t{}".format(random.choice(self.dict_platos["P"])))
		else:
			print("Debe haber al menos 2 platos de cada tipo")