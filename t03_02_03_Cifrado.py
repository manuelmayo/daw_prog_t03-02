#t03_02_03_Cifrado
		
#<CIFRADO>
class Cifrado:

	abc = "ABCDEFGHIJKLMNOPQRSTUWXYZ"
	key = ""

	def __init__(self):
		self.keyabc = self.key+"".join(
			(w for w in self.abc if w not in self.key))

	#self, string => string 
	def cifrar(self,text):
		parejas = dict(zip(self.abc,self.keyabc))
		tcifrado = ""
		for c in text.upper():
			if c in self.abc:
				tcifrado += parejas[c]
			else:
				tcifrado += c
		return tcifrado

	#self, string => string
	def descifrar(self,text):
		parejas = dict(zip(self.keyabc,self.abc))
		tdescifrado = ""
		for c in text.upper():
			if c in self.abc:
				tdescifrado += parejas[c]
			else:
				tdescifrado += c
		return tdescifrado

#<ALGORITMO CESAR>
class Cesar(Cifrado):
	
	#self, [int] => None
	def __init__(self, ndesp=3):
		self.ndesp = ndesp
		self.key = self.abc[self.ndesp:]
		super().__init__()

#<ALGORITMO CIPHER>
class Cipher(Cifrado):

	#self, [int] => None
	def __init__(self, key):
		self.key = key.upper()
		super().__init__()

t1 = "HOLA MUNDO"
t2 = "Zapato"


print("\n***CESAR***")

a1 = Cesar(3)

t1cifrado = a1.cifrar(t1)
print("{} => {}".format(t1,t1cifrado))
print("{} => {}".format(t1cifrado,a1.descifrar(t1cifrado)))

t2cifrado = a1.cifrar(t2)
print("{} => {}".format(t2,t2cifrado))
print("{} => {}".format(t2cifrado,a1.descifrar(t2cifrado)))

print("\n***CIPHER***")

a2 = Cipher("ZAFIRO")

t1cifrado = a2.cifrar(t1)
print("{} => {}".format(t1,t1cifrado))
print("{} => {}".format(t1cifrado,a2.descifrar(t1cifrado)))

t2cifrado = a2.cifrar(t2)
print("{} => {}".format(t2,t2cifrado))
print("{} => {}".format(t2cifrado,a2.descifrar(t2cifrado)))


