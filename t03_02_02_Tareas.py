#t03_02_02_Tareas

import os
import platform

_id = 0

#<TAREA>
class Tarea:
	
	_tid = 0
	#self, string, int => None
	def __init__(self,nombre,prio):
		if prio in range(6):
			Tarea._tid += 1
			self.id = Tarea._tid
			self.nombre = nombre
			self.prioridad = prio
		else:
			raise ValueError("La prioridad debe ser un entero "
							 "entre 0 y 5")
		
	#self => int
	def get_prioridad(self):
		return self.prioridad
		
	#self, int => None
	def set_prioridad(self,newprio):
		if newprio in range(6):
			self.prioridad = newprio
		else:
			raise ValueError("La prioridad debe ser un entero "
							 "entre 0 y 5")
							 
	#self => string
	def __str__(self):
		return ("{:.<25}{}\n{:.<25}{}\n{:.<25}{}\n".format(
				"ID:",self.id,"NOMBRE:",self.nombre,
				"PRIORIDAD:",self.prioridad,))
		
#<COLA>					 
class Cola:
	
	#self, data-structure => None
	def __init__(self,ltareas=[]):
		if type(ltareas) in (list,set,tuple):
			btareas = (type(x) is Tarea for x in ltareas)
			if all(btareas):
				self.tareas = list(ltareas)
			else:
				raise ValueError("El argumento debe ser una lista, "
								 "tupla o secuencia de Tareas")
		else:
			raise ValueError("El argumento debe ser una lista, tupla " 
							 "o secuencia")
	
	#self => None
	def print_title(self):
		print("\n{0} {1} {0}\n".format("#"*12,"COLA DE TAREAS"))
		
	#self => None
	def print_options(self):
		print("1) Añadir Tarea")
		print("2) Consultar Tareas")
		print("3) Consultar Tarea de mayor prioridad")
		print("4) Consultar número de tareas")
		print("5) Extraer Tarea")
		print("6) Salir")
							 
	#self, Tarea => None
	def add_tarea(self,tarea=0):
		if tarea:
			if type(tarea) is Tarea:
				self.tareas.append(tarea)
			else:
				raise ValueError("El argumento debe ser una <Tarea>")
		else:
			nombre = input("{:.<25}".format("Nombre:"))
			if nombre:
				prioridad = input_("{:.<25}".format("Prioridad:"))
				if prioridad.isdigit() and int(prioridad) in range(0,6):
					tarea = Tarea(nombre,int(prioridad))
					self.tareas.append(tarea)
					return 1
				else:
					print("\nDebes introducir una prioridad válida (0-5)")
					return 0
			else:
				print("\nLa Tarea debe tener un nombre")
				return 0
				
	#self, Boolean => List
	def get_tareas(self,max_prio=0):
		if self.tareas:
			if max_prio:
				max_prio = max([t.get_prioridad() for t in self.tareas])
				tareas = [t for t in self.tareas if 
							t.get_prioridad() == max_prio]
			else:
				tareas = self.tareas
		else:
			tareas = []
		return tareas
			
	#self => Tarea
	def extraer_tarea(self):
		list_max_prio = self.get_tareas(max_prio=1)
		if list_max_prio:
			indice_tarea = self.tareas.index(list_max_prio[0])
			return self.tareas.pop(indice_tarea)
		else:
			raise Exception("La cola está vacía")
			
	#self => Int
	def num_tareas(self):
		return len(self.tareas)
	
	#self => Int
	def __len__(self):
		return len(self.tareas)
		
#string => string
def input_(text):
	try:
		sinput = input(text)
		return sinput
	except KeyboardInterrupt:
		print("\n\nAdiós!")
		exit()
	except:
		print("\nAdios")
		exit()
		
#None => None
def clear():
	if platform.system() == "Windows":
		os.system("cls")
	elif platform.system() == "Linux":
		os.system("clear")
		
#MAIN
if __name__ == "__main__":
	
	cola_tareas = Cola()
	
	while 1:
		clear()
		cola_tareas.print_title()
		cola_tareas.print_options()
		option = input_("\n> ")
		if option.isdigit() and int(option) in range(1,7):
			if option == "1":
				print()
				if cola_tareas.add_tarea():
					print("\nTarea creada con éxito")
				else:
					print("\nError en la creación de la Tarea")
				input_("\nPulse ENTER para continuar...")
			elif option == "2":
				print()
				ltareas = cola_tareas.get_tareas()
				if ltareas:
					for t in ltareas:
						print(t)
				else:
					print("La Cola de Tareas está vacía")
				input_("\nPulse ENTER para continuar...")
			elif option == "3":
				print()
				ltareas = cola_tareas.get_tareas(max_prio=1)
				if ltareas:
					for t in ltareas:
						print(t)
				else:
					print("La Cola de Tareas está vacía")
				input_("\nPulse ENTER para continuar...")
			elif option == "4":
				print()
				print("{:.<25}{}".format("Número de Tareas:",
					  len(cola_tareas)))
				input_("\nPulse ENTER para continuar...")
			elif option == "5":
				print()
				try:
					extarea = cola_tareas.extraer_tarea()
					print(extarea)
				except:
					print("La Cola de Tareas está vacía")
				input_("\nPulse ENTER para continuar...")
			elif option == "6":
				print("\nAdiós!")
				exit()
			
		
	
